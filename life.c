#include <stdio.h>
#include <ncurses.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <locale.h>
#define M 1024
#define N 1024
typedef unsigned char (map_t[M])[N];

#define getcol(map,x,y) ((*(map))[(y)][(x)] >> 7)


#define setcol(map, x, y, c) do {register unsigned char _r = ((*(map))[(y)][(x)]); (*(map))[(y)][(x)] = (c ? (_r|0x80) : (_r&~0x80));} while(0)
#define setblack(map, x, y) (void)((*(map))[(y)][(x)] |= 0x80)
#define setwhite(map, x, y) (void)((*(map))[(y)][(x)] &= ~0x80)

void init(map_t* map) {
	//printf("size = %ld\n", sizeof(*map));
	int i, j;
	for(i=0; i<M; i++) {
		for(j=0; j<N; j++) {
			(*map)[i][j] |= (rand()%2== 0) << 7;
		}
	}
	printf("end\n");
}

void onesum(map_t *map, int x, int y) {
	int sum = 0;
	switch(x) {
		case 0:
			sum += getcol(map, x+1, y);
			switch(y) {
				case 0: sum += getcol(map, x, y+1) + getcol(map, x+1, y+1); break;
				case M-1: sum += getcol(map, x, y-1) + getcol(map, x+1, y-1); break;
				default: sum += getcol(map, x, y+1) + getcol(map, x+1, y+1) + getcol(map, x, y-1) + getcol(map, x+1, y-1); break;
			}
			break;
		case N-1:
			sum += getcol(map, x-1, y);
			switch(y) {
				case 0: sum += getcol(map, x-1, y+1) + getcol(map, x, y+1); break;
				case M-1: sum += getcol(map, x-1, y-1) + getcol(map, x, y-1); break;
				default: sum += getcol(map, x-1, y+1) + getcol(map, x, y+1) + getcol(map, x-1, y-1) + getcol(map, x, y-1); break;
			}
			break;
		default:;
			sum += getcol(map, x-1, y) + getcol(map, x+1, y);
			switch(y) {
				case 0: sum += getcol(map, x-1, y+1) + getcol(map, x, y+1) + getcol(map, x+1, y+1); break;
				case M-1: sum += getcol(map, x-1, y-1) + getcol(map, x, y-1) + getcol(map, x+1, y-1); break;
				default: sum += getcol(map, x-1, y+1) + getcol(map, x, y+1) + getcol(map, x+1, y+1)
					 + getcol(map, x-1, y-1) + getcol(map, x, y-1) + getcol(map, x+1, y-1);break;
			}
			break;
	}
	(*map)[y][x] &= 0x80; //clear right 7 bits
	(*map)[y][x] |= sum; // do not change left 1 bit
}

void update(map_t *map) {
	int i, j;
	for(i=0; i<M; i++) {
		for(j=0; j<N; j++) {
			onesum(map, j, i);
		}
	}
	for(i=0; i<M; i++) {
		for(j=0; j<N; j++) {
			switch((*map)[i][j] & ~0x80) {
				case 3:
					setblack(map, j, i);
					break;
				case 0:
				case 1:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
					if(rand()%100000 == 0)
						setblack(map, j, i);
					else 
						setwhite(map, j, i);
					break;
			}
		}
	}
}

void viewyx(int* y, int* x) {
	int rows, cols;
	getmaxyx(stdscr, rows, cols);
	cols /=2;
	timeout(50);
	int key = getch();
	switch(key) {
		//case 'h': if(*x > 5) *x -= 5; break;
		//case 'l': if(*x + cols < N - 5) *x += 5; break;
		//case 'k': if(*y > 5) *y -= 5; break;
		//case 'j': if(*y + rows < M -5) *y += 5; break;
		case 'h': if(*x > 5) *x -= 5; break;
		case 'l': if(*x + cols < N - 5) *x += 5; break;
		case 'k': if(*y > 5) *y -= 5; break;
		case 'j': if(*y + rows < M -5) *y += 5; break;
	}
}

void paint(map_t *map, int y, int x) {
	int row, col;
	assert(x < N && y < M);
	int i, j , maxi, maxj;
	getmaxyx(stdscr, row, col);
	col /= 2;
	maxi = N-x > col? col : N-x;
	maxj = M-y > row? row : M-y;
	for(j=0; j<maxj; j++) {
		move(j+1, 0);
		for(i=0; i<maxi; i++) {
			addstr(getcol(map, i+x, j+y)? "GD" : "  ");
		}
	}
}
int main() {
	int x = 0, y = 0;
	srand(time(NULL));
	initscr();
	map_t* map = malloc(M*N);
	init(map);
	while(1) {
		viewyx(&y, &x);
		mvprintw(0,0,"%04d %04d", y, x);
		refresh();
		paint(map, y, x);
		refresh();
		update(map);
	}
	getch();
	endwin();
}
